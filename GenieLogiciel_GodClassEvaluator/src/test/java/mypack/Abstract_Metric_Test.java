/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mypack;

import Metrics.ATFD_Metric;
import static junit.framework.TestCase.assertEquals;
import org.junit.Test;

/**
 *
 * @author Thomas
 */
public class Abstract_Metric_Test
{
    @Test
    public void testAFTD_Metric ()
    {
        ATFD_Metric AFTDMet = new ATFD_Metric();
        
        AFTDMet.setValue(0);
        assertEquals(AFTDMet.getValue(), 0, 0);
        
        AFTDMet.setValue(35);
        assertEquals(AFTDMet.getValue(), 35, 0.1);
        
        
        AFTDMet.setValue(12.3);
        assertEquals(AFTDMet.getValue(), 12.3, 0.1);
 
    }
}
