/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mypack;

import Metrics.WMC_Metric;
import static junit.framework.TestCase.assertEquals;
import org.junit.Test;

/**
 *
 * @author Thomas
 */
public class WMC_Metric_Test
{
    @Test
    public void testWMC_Metric ()
    {
        WMC_Metric WMCMet = new WMC_Metric();
        
        assertEquals(WMCMet.getValue(), 0, 0);
    }
    
    @Test
    public void test_IncFctNumber ()
    {
        WMC_Metric WMCMet = new WMC_Metric();
        
        WMCMet.IncFctNumber();
        
        assertEquals(WMCMet.getValue(), WMC_Metric.FUNCTION_METRIC, 0.1);
        
        WMCMet.IncFctNumber();
        WMCMet.IncFctNumber();
        WMCMet.IncFctNumber();
        
        assertEquals(WMCMet.getValue(), 4*WMC_Metric.FUNCTION_METRIC, 0.1);
    }
    
    
    @Test
    public void test_IncConstructorNumber ()
    {
        WMC_Metric WMCMet = new WMC_Metric();
        
        WMCMet.IncConstructorNumber();
        
        assertEquals(WMCMet.getValue(), WMC_Metric.CONSTRUCTOR_METRIC, 0.1);
        
        WMCMet.IncConstructorNumber();
        WMCMet.IncConstructorNumber();
        WMCMet.IncConstructorNumber();
        
        assertEquals(WMCMet.getValue(), 4*WMC_Metric.CONSTRUCTOR_METRIC, 0.1);
    }
    
    @Test
    public void testgetLowLimit()
    {
        WMC_Metric WMCMet = new WMC_Metric();
        assertEquals(20, WMCMet.getLowLimit(), 0.1);
    }
    
    @Test
    public void testgetHighLimit()
    {
        WMC_Metric WMCMet = new WMC_Metric();
        assertEquals(47, WMCMet.getHighLimit(), 0.1);
    }
    
    @Test
    public void testgetBestType()
    {
        WMC_Metric WMCMet = new WMC_Metric();
        assertEquals(true, WMCMet.getBestType());
    }
}
