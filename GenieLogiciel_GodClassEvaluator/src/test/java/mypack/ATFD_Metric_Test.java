/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mypack;

import Metrics.ATFD_Metric;
import static junit.framework.TestCase.assertEquals;
import org.junit.Test;

/**
 *
 * @author Thomas
 */
public class ATFD_Metric_Test
{
    @Test
    public void testATFD_Metric ()
    {
        ATFD_Metric ATFDMet = new ATFD_Metric();
        
        assertEquals(ATFDMet.getValue(), 0, 0);
    }
    
    @Test
    public void testgetLowLimit()
    {
        ATFD_Metric ATFDMet = new ATFD_Metric();
        assertEquals(3, ATFDMet.getLowLimit(), 0.1);
    }
    
    @Test
    public void testgetHighLimit()
    {
        ATFD_Metric ATFDMet = new ATFD_Metric();
        assertEquals(6, ATFDMet.getHighLimit(), 0.1);
    }
    
    @Test
    public void testgetBestType()
    {
        ATFD_Metric ATFDMet = new ATFD_Metric();
        assertEquals(true, ATFDMet.getBestType());
    }
}
