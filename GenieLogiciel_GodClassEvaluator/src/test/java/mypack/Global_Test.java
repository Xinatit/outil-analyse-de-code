/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mypack;

import GodClass.GodClass_Evaluator;
import Metrics.ATFD_Metric;
import Metrics.Abstract_Metric;
import Metrics.TCC_Metric;
import Metrics.WMC_Metric;
import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import static junit.framework.TestCase.assertEquals;
import org.junit.Test;
/**
 *
 * @author Thomas
 */
public class Global_Test
{
    @Test
    public void BeanJDBC_test ()
    {
        try 
        {
            FileInputStream in;
                    
            if (System.getProperty("os.name").toLowerCase().contains("windows")==true)
            {
                in = new FileInputStream("ClassExample\\BeanJDBC.java");
            }
            else
            {
                in = new FileInputStream("ClassExample//BeanJDBC.java");
            }
            
            CompilationUnit cu;
            // parse the file
            cu = JavaParser.parse(in);
            in.close();
            
            ATFD_Metric ATFDMet = new ATFD_Metric();
            ATFDMet.CalculateMetric(cu);
            assertEquals(2, ATFDMet.getValue(), 0.1); 

            WMC_Metric WMCMet = new WMC_Metric();
            WMCMet.CalculateMetric(cu);
            assertEquals(13, WMCMet.getValue(), 0.1); 
            
            TCC_Metric TCCMet = new TCC_Metric();
            TCCMet.CalculateMetric(cu);
            assertEquals(0.75, TCCMet.getValue(), 0.1); 
            
            Vector<Abstract_Metric> Metrics = new Vector<Abstract_Metric>();
            Metrics.add(ATFDMet);
            Metrics.add(WMCMet);
            Metrics.add(TCCMet);
            int GodValue = GodClass_Evaluator.GetJudgmentValue(Metrics );
            assertEquals(5, GodValue, 0); 
        } 
        catch (FileNotFoundException ex) {
            Logger.getLogger(Global_Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(Global_Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Global_Test.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        /*Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        System.out.println("Current relative path is: " + s);
        
        File curDir = new File(currentRelativePath.toAbsolutePath().toString());
        File[] filesList = curDir.listFiles();
        for(File f : filesList){
                System.out.println(f.getName());
        };
        
        System.out.println(currentRelativePath.toAbsolutePath().toString()+"//ClassExample");
         curDir = new File(currentRelativePath.toAbsolutePath().toString()+"//ClassExample");
         filesList = curDir.listFiles();
        for(File f : filesList){
                System.out.println(f.getName());
        };*/

    }
    
    
    @Test
    public void PSK_test ()
    {
        try 
        {
            FileInputStream in;
            if (System.getProperty("os.name").toLowerCase().contains("windows")==true)
            {
                in = new FileInputStream("ClassExample\\PSK.java");
            }
            else
            {
                in = new FileInputStream("ClassExample//PSK.java");
            }
            
            CompilationUnit cu;
            // parse the file
            cu = JavaParser.parse(in);
            in.close();
            
            ATFD_Metric ATFDMet = new ATFD_Metric();
            ATFDMet.CalculateMetric(cu);
            assertEquals(55, ATFDMet.getValue(), 0.1); 

            WMC_Metric WMCMet = new WMC_Metric();
            WMCMet.CalculateMetric(cu);
            assertEquals(WMCMet.getValue(), 46.5, 0.1); 
            
            TCC_Metric TCCMet = new TCC_Metric();
            TCCMet.CalculateMetric(cu);
            assertEquals(TCCMet.getValue(), 0, 0.1); 

            Vector<Abstract_Metric> Metrics = new Vector<Abstract_Metric>();
            Metrics.add(ATFDMet);
            Metrics.add(WMCMet);
            Metrics.add(TCCMet);
            int GodValue = GodClass_Evaluator.GetJudgmentValue(Metrics );
            assertEquals(GodValue, 25, 0); 
        } 
        catch (FileNotFoundException ex) {
            Logger.getLogger(Global_Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(Global_Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Global_Test.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    
    
    @Test
    public void A_test ()
    {
        try 
        {
            FileInputStream in;
            if (System.getProperty("os.name").toLowerCase().contains("windows")==true)
            {
                in = new FileInputStream("ClassExample\\A.java");
            }
            else
            {
                in = new FileInputStream("ClassExample//A.java");
            }
            
            CompilationUnit cu;
            // parse the file
            cu = JavaParser.parse(in);
            in.close();
            
            ATFD_Metric ATFDMet = new ATFD_Metric();
            ATFDMet.CalculateMetric(cu);
            assertEquals(ATFDMet.getValue(), 3, 0.1); 

            WMC_Metric WMCMet = new WMC_Metric();
            WMCMet.CalculateMetric(cu);
            assertEquals(WMCMet.getValue(), 7, 0.1); 
            
            TCC_Metric TCCMet = new TCC_Metric();
            TCCMet.CalculateMetric(cu);
            assertEquals(TCCMet.getValue(), 0.5, 0.1); 

            Vector<Abstract_Metric> Metrics = new Vector<Abstract_Metric>();
            Metrics.add(ATFDMet);
            Metrics.add(WMCMet);
            Metrics.add(TCCMet);
            int GodValue = GodClass_Evaluator.GetJudgmentValue(Metrics );
            assertEquals(GodValue, 5, 0); 
        } 
        catch (FileNotFoundException ex) {
            Logger.getLogger(Global_Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(Global_Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Global_Test.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
