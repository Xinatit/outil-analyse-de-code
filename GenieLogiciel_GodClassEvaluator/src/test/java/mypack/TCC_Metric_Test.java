/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mypack;

import Metrics.TCC_Metric;
import static junit.framework.TestCase.assertEquals;
import org.junit.Test;

/**
 *
 * @author Julien
 */
public class TCC_Metric_Test {
    
    @Test
    public void testTCC_Metric ()
    {
        TCC_Metric TCCMet = new TCC_Metric();
        
        assertEquals(TCCMet.getValue(), 0, 0);
    }
    
    @Test
    public void test_IncFctNumber ()
    {
        TCC_Metric TCCMet = new TCC_Metric();
        
        TCCMet.IncFctNumber();
        
        assertEquals(TCCMet.getValue(), TCC_Metric.FUNCTION_METRIC, 0.1);
        
        TCCMet.IncFctNumber();
        TCCMet.IncFctNumber();
        TCCMet.IncFctNumber();
        
        assertEquals(TCCMet.getValue(), 4*TCC_Metric.FUNCTION_METRIC, 0.1);
    }
    
    @Test
    public void testgetLowLimit()
    {
        TCC_Metric TCCMet = new TCC_Metric();
        assertEquals(0.3, TCCMet.getLowLimit(), 0.01);
    }
    
    @Test
    public void testgetHighLimit()
    {
        TCC_Metric TCCMet = new TCC_Metric();
        assertEquals(0.8, TCCMet.getHighLimit(), 0.01);
    }
    
    @Test
    public void testgetBestType()
    {
        TCC_Metric TCCMet = new TCC_Metric();
        assertEquals(false, TCCMet.getBestType());
    }

}
