/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mypack;

import GodClass.GodClass_Evaluator;
import Metrics.Abstract_Metric;
import java.util.Vector;
import static junit.framework.TestCase.assertEquals;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Thomas
 */
@RunWith(MockitoJUnitRunner.class) public class GodClass_Evaluator_Test
{
    @Mock
    Abstract_Metric MetOne;
    
    @Mock
    Abstract_Metric MetTwo;
    
    @Mock
    Abstract_Metric MetThree;
    
    
    
    @After
    public void tearDown() {}
    
    
    @Test
    public void testGetJudgmentValue ()
    {
        when(MetOne.getLowLimit()).thenReturn(5.0);
        when(MetOne.getHighLimit()).thenReturn(10.0);
        when(MetOne.getBestType()).thenReturn(true);
        
        when(MetTwo.getLowLimit()).thenReturn(0.3);
        when(MetTwo.getHighLimit()).thenReturn(0.5);
        when(MetTwo.getBestType()).thenReturn(false);
        
        when(MetThree.getLowLimit()).thenReturn(5.0);
        when(MetThree.getHighLimit()).thenReturn(10.0);
        when(MetThree.getBestType()).thenReturn(true);
        
        
        when(MetOne.getValue()).thenReturn(5.0);
        when(MetTwo.getValue()).thenReturn(0.5);
        when(MetThree.getValue()).thenReturn(5.0);
        Vector<Abstract_Metric> Metrics = new Vector<Abstract_Metric>();
        Metrics.add(MetOne);
        Metrics.add(MetTwo);
        Metrics.add(MetThree);
        int result = GodClass_Evaluator.GetJudgmentValue(Metrics);
        assertEquals(0, result);
 
        
        when(MetOne.getValue()).thenReturn(10.0);
        when(MetTwo.getValue()).thenReturn(0.5);
        when(MetThree.getValue()).thenReturn(5.0);
        Metrics = new Vector<Abstract_Metric>();
        Metrics.add(MetOne);
        Metrics.add(MetTwo);
        Metrics.add(MetThree);
        result = GodClass_Evaluator.GetJudgmentValue(Metrics);
        assertEquals(5, result);
        
        
        when(MetOne.getValue()).thenReturn(10.0);
        when(MetTwo.getValue()).thenReturn(0.5);
        when(MetThree.getValue()).thenReturn(10.0);
        Metrics = new Vector<Abstract_Metric>();
        Metrics.add(MetOne);
        Metrics.add(MetTwo);
        Metrics.add(MetThree);
        result = GodClass_Evaluator.GetJudgmentValue(Metrics);
        assertEquals(10, result);
        
        
        when(MetOne.getValue()).thenReturn(6.0);
        when(MetTwo.getValue()).thenReturn(0.3);
        when(MetThree.getValue()).thenReturn(6.0);
        Metrics = new Vector<Abstract_Metric>();
        Metrics.add(MetOne);
        Metrics.add(MetTwo);
        Metrics.add(MetThree);
        result = GodClass_Evaluator.GetJudgmentValue(Metrics);
        assertEquals(15, result);
        
        
        //------
        
        
        when(MetOne.getValue()).thenReturn(11.0);
        when(MetTwo.getValue()).thenReturn(0.5);
        when(MetThree.getValue()).thenReturn(5.0);
        Metrics = new Vector<Abstract_Metric>();
        Metrics.add(MetOne);
        Metrics.add(MetTwo);
        Metrics.add(MetThree);
        result = GodClass_Evaluator.GetJudgmentValue(Metrics);
        assertEquals(10, result);
        
        
        when(MetOne.getValue()).thenReturn(11.0);
        when(MetTwo.getValue()).thenReturn(0.5);
        when(MetThree.getValue()).thenReturn(11.0);
        Metrics = new Vector<Abstract_Metric>();
        Metrics.add(MetOne);
        Metrics.add(MetTwo);
        Metrics.add(MetThree);
        result = GodClass_Evaluator.GetJudgmentValue(Metrics);
        assertEquals(20, result);
        
        
        when(MetOne.getValue()).thenReturn(11.0);
        when(MetTwo.getValue()).thenReturn(0.1);
        when(MetThree.getValue()).thenReturn(11.0);
        Metrics = new Vector<Abstract_Metric>();
        Metrics.add(MetOne);
        Metrics.add(MetTwo);
        Metrics.add(MetThree);
        result = GodClass_Evaluator.GetJudgmentValue(Metrics);
        assertEquals(30, result);
    }
    
    
    @Test
    public void GetJudgmentComment ()
    {
        String restring = GodClass_Evaluator.GetJudgmentComment(0);
        assertEquals("Not a god at all !", restring);
        
        restring = GodClass_Evaluator.GetJudgmentComment(5);
        assertEquals("Not a god !", restring);
        
        restring = GodClass_Evaluator.GetJudgmentComment(10);
        assertEquals("Demigod !", restring);
        
        restring = GodClass_Evaluator.GetJudgmentComment(15);
        assertEquals("Demigod !", restring);
        
        restring = GodClass_Evaluator.GetJudgmentComment(20);
        assertEquals("God !", restring);
        
        restring = GodClass_Evaluator.GetJudgmentComment(25);
        assertEquals("God !", restring);
        
        restring = GodClass_Evaluator.GetJudgmentComment(30);
        assertEquals("God of gods !", restring);
        
        //Test des valeurs qui doivent donner "Error, bad value !"
        int cpt = 1;
        
        while (cpt<39)
        {
            if (cpt%5!=0)
            {
                restring = GodClass_Evaluator.GetJudgmentComment(cpt);
                assertEquals("Error, bad value !", restring);
            }
            else
            {
                if ((cpt%5)==0 && cpt>30)
                {
                    restring = GodClass_Evaluator.GetJudgmentComment(cpt);
                assertEquals("God of gods !", restring);
                }
            }
            
            cpt++;
        }
    }
    
    
    @Test
    public void testGetMetricComment()
    {
        when(MetOne.getLowLimit()).thenReturn(5.0);
        when(MetOne.getHighLimit()).thenReturn(10.0);
        when(MetOne.getBestType()).thenReturn(true);
        
        when(MetOne.getValue()).thenReturn(11.0);
        assertEquals("To high", GodClass_Evaluator.GetMetricComment(MetOne));
        
        when(MetOne.getValue()).thenReturn(10.0);
        assertEquals("Middle", GodClass_Evaluator.GetMetricComment(MetOne));
        
        when(MetOne.getValue()).thenReturn(5.0);
        assertEquals("Good", GodClass_Evaluator.GetMetricComment(MetOne));
        
        
        
        when(MetOne.getLowLimit()).thenReturn(0.3);
        when(MetOne.getHighLimit()).thenReturn(0.5);
        when(MetOne.getBestType()).thenReturn(false);
        
        when(MetOne.getValue()).thenReturn(0.29);
        assertEquals("To high", GodClass_Evaluator.GetMetricComment(MetOne));
        
        when(MetOne.getValue()).thenReturn(0.3);
        assertEquals("Middle", GodClass_Evaluator.GetMetricComment(MetOne));
        
        when(MetOne.getValue()).thenReturn(0.5);
        assertEquals("Good", GodClass_Evaluator.GetMetricComment(MetOne));
    }
}
