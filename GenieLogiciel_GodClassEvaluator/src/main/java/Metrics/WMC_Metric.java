/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metrics;

import Metric.Visitor.ATFD_Visitor;
import Metric.Visitor.WMC_Visitor;
import japa.parser.ast.CompilationUnit;
import java.util.Vector;

/**
 *
 * @author Thomas
* 
 * How it works : count the number of function (it's possible to have a metric for the function complexity)
 * 
 * Function that are counted :
 * - "Classic" function
 * - Setter/Getter
 * - Constructor
 * - 
 * 
 * Function that are not counted :
 * 
 * 
 * Complexity :
 * - 0.5 for the constructor
 * - 1 for all the function
 * 
 * 
 * 
 * 
 * 
 * Todo :
 * - [Idée] Mieux compter les constructeur. Genre si il y en as avec + de 5 paramètre mettre un métrique plus haut
 * - [Idée] mettre une valeur pour les Eventhandler différente des fonctions
 */

public class WMC_Metric extends Abstract_Metric
{
    private WMC_Visitor ElVisitor;
    
    public static final float FUNCTION_METRIC = 1;
    public static final float CONSTRUCTOR_METRIC = (float)0.5;
    
    public WMC_Metric ()
    {
        super();
        
        ElVisitor = new WMC_Visitor ();
    }
    
    public void IncFctNumber ()
    {
        Value = FUNCTION_METRIC + Value;
    }
    
    public void IncConstructorNumber ()
    {
        Value = CONSTRUCTOR_METRIC + Value;
    }
    

    @Override
    public void CalculateMetric(CompilationUnit cu) {
        ElVisitor.visit (cu, this);
    }

    @Override
    public double getLowLimit() {
        return 20;
    }

    @Override
    public double getHighLimit() {
        return 47;
    }

    @Override
    public boolean getBestType() {
        return true;
    }
    
}
