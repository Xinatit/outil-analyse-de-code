/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metrics;

import Metric.Visitor.TCC_Visitor;
import japa.parser.ast.CompilationUnit;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 *
 * @author Julien
 */
public class TCC_Metric extends Abstract_Metric
{
    private TCC_Visitor ElVisitor;
    
    private Vector vVariableInClass,vNameMethod,vBodyMethod;
    private double NP,NDC;
    public static final float FUNCTION_METRIC = 1;
    public static final float CONSTRUCTOR_METRIC = (float)0.5;
    
    
    public TCC_Metric()
    {
        super();
        ElVisitor = new TCC_Visitor();
        vVariableInClass = new Vector();
        vNameMethod = new Vector();
        vBodyMethod = new Vector();
    }


    @Override
    public void CalculateMetric(CompilationUnit cu) {
        ElVisitor.visit (cu, this);
        getTCCValue();
    }

    public void IncFctNumber() 
    {
        Value = FUNCTION_METRIC + Value;
    }
    
    
    //NECESSITE AMELIORATION :
        /*
        -VARIABLE DECLARE DANS LE BODY MEME QUE NOM VARIABLE CLASSE
        -APPELLE METHODE DANS UNE METHODE , ORDRE?
*/
    private void getTCCValue()
    {
        NP=Value;
        int cpt =0;
        Vector vStockageNomMethodeTemp = new Vector();
        boolean methodeInMethode = false;
        for(int j=0;j<vVariableInClass.size();j++)
        {
            vStockageNomMethodeTemp = new Vector();
            for(int i=0;i<vBodyMethod.size();i++)
            {
                StringTokenizer st = new StringTokenizer(vBodyMethod.elementAt(i).toString(),"  \t\n\r\f;().{}");
                while(st.hasMoreTokens())
                {
                    String stToken = st.nextToken();
                    if(vVariableInClass.elementAt(j).toString().compareTo(stToken) == 0)
                    {
                        cpt++;
                        vStockageNomMethodeTemp.add(vNameMethod.elementAt(i).toString());
                        break;
                    }
                    else
                    {
                        for(int k = 0;k<vStockageNomMethodeTemp.size();k++)
                        {
                            if(vStockageNomMethodeTemp.elementAt(k).toString().compareTo(stToken) == 0)
                            {
                                cpt++;
                                methodeInMethode = true;
                                break;
                            }
                        }
                        if(methodeInMethode == true)
                        {
                            methodeInMethode = false;
                            break;
                        }
                    }
                }
            }
            if(cpt%2 == 0)
            {
                NDC = cpt/2.0 + NDC;
            }
            else
            {  
                NDC = (cpt-1)/2.0 + NDC;
            }
            cpt=0;
        }            

        if(NP != 0)
        {
            Value = NDC/NP;
        }
        else
        {
            System.out.println("NDC = 0 donc aucune méthode connectée");
            Value = 0;
        }
        System.out.println(Value);
    }
    public void VectorVariable(String a)
    {
        vVariableInClass.add(a);
    }
   
   public void VectorNameMethode(String a)
   {
       vNameMethod.add(a);
   }
   public void VectorBodyMethode(String a)
   {
       vBodyMethod.add(a);
   }

    @Override
    public double getLowLimit() {
        return 0.3;
    }

    @Override
    public double getHighLimit() {
        return 0.8;
    }

    @Override
    public boolean getBestType() {
        return false;
    }
}
