/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metrics;

import Metric.Visitor.Abstract_Visitor;
import japa.parser.ast.CompilationUnit;

/**
 *
 * @author Thomas
 * 
 * On pourrauis peut être liquider certaines méthones non utilisées.
 */

//https://www.simpleorientedarchitecture.com/how-to-identify-feature-envy-using-ndepend/

public abstract class Abstract_Metric 
{
    //The value of the metric
    protected double Value;
    //protected Abstract_Visitor ElVisitor;
    
    public Abstract_Metric ()
    {
        Value = 0;
        //ElVisitor = null;
    }
    
    public double getValue ()
    {
        return Value;
    }
    
    public void setValue (double pValue)
    {
        Value = pValue;
    }
    
    
    public abstract void CalculateMetric (CompilationUnit cu);
    
    //This must return the "LowLimit" for the metric.
    public abstract double getLowLimit();
    
    //This must return the "HighLimit" fro the metric.
    public abstract double getHighLimit();
    
    //For some metrics, a higer value is better and for others a lower value is best.
    //True mean that a value under the "LowLimit" is better.
    //False mean that a value upper the "High" is better.
    public abstract boolean getBestType();
}
