/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metrics;

import Metric.Visitor.ATFD_Visitor;
import Metric.Visitor.Abstract_Visitor;
import japa.parser.ast.CompilationUnit;
import java.util.Vector;

/**
 *
 * @author Thomas
 */
public class ATFD_Metric extends Abstract_Metric
{
    private ATFD_Visitor ElVisitor;
    
    private Vector vClassesTripotees;   //Contient la liste des classes qui se laissent tripotées les variables membres par la classe mesurée.
    
    public ATFD_Metric ()
    {
        super();
        
        ElVisitor = new ATFD_Visitor ();
        
        vClassesTripotees = new Vector();
    }
    
    @Override
    public void CalculateMetric(CompilationUnit CompiUni)
    {
        ElVisitor.visit (CompiUni, this);
        
        Value = vClassesTripotees.size();
    }
    
    public void AddClass (String ClassName)
    {
        if (vClassesTripotees.contains(ClassName) == false)
        {
            vClassesTripotees.add(ClassName);
            System.out.println("Nouvelle classe tripotée : " + ClassName);
        }
    }

    @Override
    public double getLowLimit() {
        return 3;
    }

    @Override
    public double getHighLimit() {
        return 6;
    }

    @Override
    public boolean getBestType() {
        return true;
    }
    
}
