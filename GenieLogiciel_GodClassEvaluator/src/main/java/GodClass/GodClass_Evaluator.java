/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GodClass;

import Metrics.Abstract_Metric;
import java.util.Vector;

/**
 *
 * @author Thomas
 */
public class GodClass_Evaluator 
{
    //https://www.simpleorientedarchitecture.com/how-to-identify-god-class-using-ndepend/
    
    public static int GetJudgmentValue (Vector<Abstract_Metric> Metrics)
    {
        int GodCount = 0;
        int HalfGodCount =0;
        
        int cpt =0;
        while (cpt<Metrics.size())
        {
            Abstract_Metric MetricInEvaluation = Metrics.get(cpt);
            
            if (MetricInEvaluation.getBestType() == true) //A value under the "LowLimit" is better.
            {
                if (MetricInEvaluation.getValue()>MetricInEvaluation.getLowLimit())
                {
                    if (MetricInEvaluation.getValue()>MetricInEvaluation.getHighLimit())
                    {
                        GodCount++;
                    }
                    else
                    {
                        HalfGodCount++;
                    }
                }
            }
            else //A value upper the "High" is better.
            {
                if (MetricInEvaluation.getValue()<MetricInEvaluation.getHighLimit())
                {
                    if (MetricInEvaluation.getValue()<MetricInEvaluation.getLowLimit())
                    {
                        GodCount++;
                    }
                    else
                    {
                        HalfGodCount++;
                    }
                }
            }
            
            cpt++;
        }
       
        return GodCount*10 + HalfGodCount*5;
    }
    
    public static String GetJudgmentComment (int ClassValue)
    {
        String comment = null;
        switch (ClassValue)
        {
            case 0 :
                comment = "Not a god at all !";
                break;
                
            case 5 :
                comment = "Not a god !";
                break;
                
            case 10 : 
            case 15 :
                comment = "Demigod !";
                break;
                
            case 20 :
            case 25 :
                comment = "God !";
                break;
                
            case 30 :
                comment = "God of gods !";
                break;
                
            default :
                if ((ClassValue%5)==0)
                {
                    comment = "God of gods !";
                }
                else
                {
                    comment = "Error, bad value !";
                }
                
                break;
            
        }
        
        return comment;
    }
    
    public static String GetMetricComment (Abstract_Metric Metric)
    {
        if (Metric.getBestType() == true) //A value under the "LowLimit" is better.
        {
            if (Metric.getValue()>Metric.getLowLimit())
            {
                if (Metric.getValue()>Metric.getHighLimit())
                {
                    return "To high";
                }
                else
                {
                    return "Middle";
                }
            }
            else
            {
                return "Good";
            }
        }
        else //A value upper the "High" is better.
        {
            if (Metric.getValue()<Metric.getHighLimit())
            {
                if (Metric.getValue()<Metric.getLowLimit())
                {
                    return "To high";
                }
                else
                {
                    return "Middle";
                }
            }
            else
            {
                return "Good";
            }
        }
    }
}
