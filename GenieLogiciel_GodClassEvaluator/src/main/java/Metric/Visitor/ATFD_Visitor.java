/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metric.Visitor;

import Metrics.ATFD_Metric;
import Metrics.Abstract_Metric;
import japa.parser.ast.body.FieldDeclaration;
import japa.parser.ast.expr.Expression;
import japa.parser.ast.expr.FieldAccessExpr;
import japa.parser.ast.expr.MethodCallExpr;
import japa.parser.ast.expr.ThisExpr;
import japa.parser.ast.type.Type;
import japa.parser.ast.visitor.VoidVisitorAdapter;
import java.util.List;

/**
 *
 * @author Thomas
 * 
 * Ce qu'on doit regarder :
 * - Les accesseurs.
 * - L'accès directe.
 */

public class ATFD_Visitor extends Abstract_Visitor
{
    //For the Mutator method (setter)
    @Override
    public void visit(MethodCallExpr MethCall, Abstract_Metric metric)
    {     
        // Si la fonction commence par "get","is" ou "set" c'est vraisemblablement un accesseur. [Attention, pour "is" on est pas sur mais c'est fort probable]
        if (GetterOrSetter (MethCall.getName(), MethCall.getArgs()))
        {
            // Il faut vérififer si c'est pas un accesseur de la classe sur laquelle on travaille.
            // On regarde si le scope (l'objet qui apelle la fonction) est différent de this.
            // Si le scope est null, on a fait un appel à l'accesseur directement. Si l'accesseur à été apellée préfixé de this, le scope est une instance de la classe "ThisExpr".
            if (MethCall.getScope() != null && (MethCall.getScope() instanceof  ThisExpr) == false)
            {
                System.out.println("ATFD : Mutator call -> " + MethCall.toString());
                ((ATFD_Metric)metric).AddClass(MethCall.getScope().toString());
            }
        }
    }
    
    @Override
    public void visit(FieldAccessExpr FAE, Abstract_Metric metric)
    {
        //On regarde si l'attribut accédé ne fait pas partie de la classe mesurée.
        //Si la variable à été apellée préfixé de this, le scope est une instance de la classe "ThisExpr"
        if ((FAE.getScope() instanceof  ThisExpr) == false)
        {
            System.out.println("ATFD : Variable access -> " + FAE.toString());
            ((ATFD_Metric)metric).AddClass(FAE.getScope().toString());
        }
    }
    
    private boolean GetterOrSetter (String Name, List<Expression> args)
    {
        Name = Name.toLowerCase();
        
        if (Name.startsWith("get") || Name.startsWith("is")) //Getter
        {
            if (args == null)
            {
                return true; //A getter doesn't have parameters
            }
        }
        else
        {
            if (Name.startsWith("set")) //Setter
            {
                if (args != null && args.size() == 1)
                {
                    return true; //A setter has 1 and only 1 parameter
                }
            }
        }
        
        return false;
    }
}
