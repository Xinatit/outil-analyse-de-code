/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metric.Visitor;

import Metrics.Abstract_Metric;
import Metrics.WMC_Metric;
import japa.parser.ast.body.ConstructorDeclaration;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.visitor.VoidVisitorAdapter;

/**
 *
 * @author Thomas
 */
public class WMC_Visitor extends Abstract_Visitor
{
    @Override
        public void visit(ConstructorDeclaration ConstruDecl, Abstract_Metric metric) {
           // here you can access the attributes of the method.
           // this method will be called for all methods in this 
           // CompilationUnit, including inner class methods
           System.out.println("WMC : Constructor declaration -> " + ConstruDecl.getName());
           ((WMC_Metric)metric).IncConstructorNumber();
        }

        @Override
        public void visit(MethodDeclaration MethDecl, Abstract_Metric metric) {
           // here you can access the attributes of the method.
           // this method will be called for all methods in this 
           // CompilationUnit, including inner class methods
           System.out.println("WMC : Method declaration -> " + MethDecl.getName());
           ((WMC_Metric)metric).IncFctNumber();
        }
}
