/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metric.Visitor;

import Metrics.Abstract_Metric;
import Metrics.TCC_Metric;
import japa.parser.ast.body.FieldDeclaration;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.visitor.VoidVisitorAdapter;
import java.lang.reflect.Modifier;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 *
 * @author Julien
 */
public class TCC_Visitor extends Abstract_Visitor
{    
    @Override
    public void visit(FieldDeclaration FieldDec, Abstract_Metric metric) {
        Object[] listeVariable = new Object[FieldDec.getVariables().size()];
        System.out.println(FieldDec.getVariables());
        FieldDec.getVariables().toArray(listeVariable);
        for(int i=0;i<listeVariable.length;i++)
        {
            StringTokenizer st = new StringTokenizer(listeVariable[i].toString()," ;,");
            while(st.hasMoreTokens())
            {
                String stToken = st.nextToken();
                if(stToken.compareTo("=") == 0 || stToken.compareTo(",") == 0)
                    break;
                else
                {
                    System.out.println("TCC : Field declaration -> " + FieldDec.toString());
                    ((TCC_Metric)metric).VectorVariable(stToken);
                }
            }
        }
    }  

    @Override
    public void visit(MethodDeclaration MethDecl, Abstract_Metric metric) {

        int modifier = MethDecl.getModifiers();
        if(Modifier.toString(modifier).compareTo("public") == 0)
        {
            System.out.println("TCC : Mutator declaration -> " + MethDecl.toString());
            ((TCC_Metric)metric).IncFctNumber();
            System.out.println(MethDecl.getName());
            ((TCC_Metric)metric).VectorNameMethode(MethDecl.getName());
            System.out.println(MethDecl.getBody().toString());
            ((TCC_Metric)metric).VectorBodyMethode(MethDecl.getBody().toString());
        }
    }
    
    
    
}
